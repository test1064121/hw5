import { IStudent } from "../interfaces/interfaces";
import { ITeacher } from "../interfaces/interfaces";
import { ISubject } from "../interfaces/interfaces";
import { Subject } from "../classes/subject";
import { Student } from "../classes/student";

export function logGrades(
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    const result = originalMethod.apply(this, args);

    console.log(`Method ${propertyKey} was called and finished its work`);

    return result;
  };

  return descriptor;
}

export function required(
  target: any,
  propertyKey: string,
  parameterIndex: number
) {
  const originalMethod = target[propertyKey];

  target[propertyKey] = function (...args: any[]) {
    const argument = args[parameterIndex];

    if (argument !== undefined && argument !== null) {
      return originalMethod.apply(this, args);
    } else {
      console.log(`Required argument is missing.`);
    }
  };

  return target;
}

export function throwErrorOnDrinkBribe(
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    try {
      return originalMethod.apply(this, args);
    } catch (error: any) {
      console.log(`Error occurred: ${error.message}`);
    }
  };

  return descriptor;
}

export function printEnrolledStudentsInfo(
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    console.log(`Executing method ${propertyKey}`);

    return originalMethod.apply(this, args);
  };

  return descriptor;
}

export function logGradesForStudent(
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (student: IStudent) {
    console.log(`Getting grade for student ${student.name} ${student.surname}`);

    return originalMethod.apply(this, [student]);
  };

  return descriptor;
}

export function validateGrade(
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor
) {
  const originalMethod = descriptor.value;

  descriptor.value = function (student: IStudent, grade: number) {
    if (grade >= 0 && grade <= 100) {
      console.log(
        `Setting grade ${grade} for student ${student.name} ${student.surname}`
      );

      return originalMethod.apply(this, [student, grade]);
    } else {
      console.log(
        `Invalid grade ${grade} for student ${student.name} ${student.surname}`
      );
    }
  };

  return descriptor;
}
