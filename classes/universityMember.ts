import { IUniversityMember } from "../interfaces/interfaces";

export abstract class UniversityMember implements IUniversityMember {
  constructor(public name: string, public surname: string) {}
}
