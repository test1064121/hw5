import { ITeacher, IStudent, ISubject } from "../interfaces/interfaces";
import { UniversityMember } from "./universityMember";
import { required } from "../decorators/decorators";

export class Teacher extends UniversityMember implements ITeacher {
  subjects: ISubject[] = [];

  constructor(name: string, surname: string) {
    super(name, surname);
  }

  setGrade(
    student: IStudent,
    @required subject: ISubject,
    grade: number
  ): void {
    if (
      subject.teacher === this &&
      subject.enrolledStudents.includes(student)
    ) {
      subject.setGradeForStudent(student, grade);
      console.log(
        `${this.name} ${this.surname} assigns a grade of ${grade} to ${student.name} ${student.surname} in ${subject.name}`
      );
    } else {
      console.log(
        `${this.name} ${this.surname} cannot assign a grade for ${student.name} ${student.surname} in ${subject.name}`
      );
    }
  }
}
