import {
  ITeacher,
  IStudent,
  ISubject,
  IUniversity,
} from "../interfaces/interfaces";
import { required } from "../decorators/decorators";

export class University implements IUniversity {
  name: string;
  teachers: ITeacher[] = [];
  students: IStudent[] = [];

  constructor(name: string) {
    this.name = name;
  }

  assignTeacher(teacher: ITeacher, @required subject: ISubject): void {
    if (!this.teachers.includes(teacher)) {
      this.teachers.push(teacher);
    }

    teacher.subjects.push(subject);
    subject.teacher = teacher;
  }

  enrollStudent(student: IStudent, @required subject: ISubject): void {
    if (!this.students.includes(student)) {
      this.students.push(student);
    }

    student.subjects.push(subject);
    subject.enrolledStudents.push(student);
  }
}
