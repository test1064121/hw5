import { IStudent, ITeacher, ISubject } from "../interfaces/interfaces";
import { BribeType } from "../types/enums";
import { UniversityMember } from "./universityMember";
import {
  logGrades,
  required,
  throwErrorOnDrinkBribe,
} from "../decorators/decorators";

export class Student extends UniversityMember implements IStudent {
  subjects: ISubject[] = [];

  constructor(name: string, surname: string) {
    super(name, surname);
  }

  @logGrades
  printGrades(): void {
    if (this.subjects.length > 0) {
      console.log(`Grades of ${this.name} ${this.surname}:`);
      this.subjects.forEach((subject) => {
        const grade = subject.getGradeForStudent(this);
        if (grade !== undefined) {
          console.log(`${subject.name}: ${grade}`);
        }
      });
    } else {
      console.log(`${this.name} ${this.surname} doesn't have any subjects yet`);
    }
  }

  offerBribe(
    teacher: ITeacher,
    type: BribeType,
    @required subject: ISubject
  ): void {
    console.log(
      `Student ${this.name} ${this.surname} offers a ${type} bribe to ${teacher.name} ${teacher.surname} for ${subject.name}`
    );

    switch (type) {
      case BribeType.Cash:
        this.processCashBribe(teacher, subject);
        break;
      case BribeType.Drink:
        try {
          this.processDrinkBribe(teacher, subject);
        } catch (error) {
          console.log(error);
        }
        break;
      default:
        console.log(
          `Teacher ${teacher.name} ${teacher.surname} does not accept ${type} bribes.`
        );
        break;
    }
  }

  private processCashBribe(
    teacher: ITeacher,
    @required subject: ISubject
  ): void {
    console.log(
      `Processing cash bribe for ${teacher.name} ${teacher.surname} in ${subject.name}`
    );
    const grade = 90;

    console.log(
      `${teacher.name} ${teacher.surname} assigns a grade of ${grade} to ${this.name} ${this.surname} in ${subject.name}`
    );
    subject.setGradeForStudent(this, grade);
  }

  @throwErrorOnDrinkBribe
  private processDrinkBribe(
    teacher: ITeacher,
    @required subject: ISubject
  ): void {
    console.log(
      `Processing drink bribe for ${teacher.name} ${teacher.surname} in ${subject.name}`
    );
    throw new Error(
      `Teacher ${teacher.name} ${teacher.surname} cannot accept a drink as a bribe.`
    );
  }
}
