import { ISubject, IStudent, ITeacher } from "../interfaces/interfaces";
import {
  logGradesForStudent,
  validateGrade,
  printEnrolledStudentsInfo,
} from "../decorators/decorators";

export class Subject implements ISubject {
  teacher: ITeacher | null = null;
  enrolledStudents: IStudent[] = [];
  private grades: Map<IStudent, number> = new Map<IStudent, number>();

  constructor(public name: string) {}

  @logGradesForStudent
  getGradeForStudent(student: IStudent): number | undefined {
    return this.grades.get(student);
  }

  @validateGrade
  setGradeForStudent(student: IStudent, grade: number): void {
    this.grades.set(student, grade);
  }

  @printEnrolledStudentsInfo
  printEnrolledStudents(): void {
    console.log(`Enrolled students in ${this.name}:`);
    this.enrolledStudents.forEach((student) => {
      console.log(`${student.name} ${student.surname}`);
    });
  }
}
