export enum Role {
  Student = "STUDENT",
  Teacher = "TEACHER",
}

export enum BribeType {
  Cash = "CASH",
  Drink = "DRINK",
  Other = "OTHER",
}
