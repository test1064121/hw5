"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BribeType = exports.Role = void 0;
var Role;
(function (Role) {
    Role["Student"] = "STUDENT";
    Role["Teacher"] = "TEACHER";
})(Role || (exports.Role = Role = {}));
var BribeType;
(function (BribeType) {
    BribeType["Cash"] = "CASH";
    BribeType["Drink"] = "DRINK";
    BribeType["Other"] = "OTHER";
})(BribeType || (exports.BribeType = BribeType = {}));
