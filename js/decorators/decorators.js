"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateGrade = exports.logGradesForStudent = exports.printEnrolledStudentsInfo = exports.throwErrorOnDrinkBribe = exports.required = exports.logGrades = void 0;
function logGrades(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        const result = originalMethod.apply(this, args);
        console.log(`Method ${propertyKey} was called and finished its work`);
        return result;
    };
    return descriptor;
}
exports.logGrades = logGrades;
function required(target, propertyKey, parameterIndex) {
    const originalMethod = target[propertyKey];
    target[propertyKey] = function (...args) {
        const argument = args[parameterIndex];
        if (argument !== undefined && argument !== null) {
            return originalMethod.apply(this, args);
        }
        else {
            console.log(`Required argument is missing.`);
        }
    };
    return target;
}
exports.required = required;
function throwErrorOnDrinkBribe(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        try {
            return originalMethod.apply(this, args);
        }
        catch (error) {
            console.log(`Error occurred: ${error.message}`);
        }
    };
    return descriptor;
}
exports.throwErrorOnDrinkBribe = throwErrorOnDrinkBribe;
function printEnrolledStudentsInfo(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        console.log(`Executing method ${propertyKey}`);
        return originalMethod.apply(this, args);
    };
    return descriptor;
}
exports.printEnrolledStudentsInfo = printEnrolledStudentsInfo;
function logGradesForStudent(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (student) {
        console.log(`Getting grade for student ${student.name} ${student.surname}`);
        return originalMethod.apply(this, [student]);
    };
    return descriptor;
}
exports.logGradesForStudent = logGradesForStudent;
function validateGrade(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (student, grade) {
        if (grade >= 0 && grade <= 100) {
            console.log(`Setting grade ${grade} for student ${student.name} ${student.surname}`);
            return originalMethod.apply(this, [student, grade]);
        }
        else {
            console.log(`Invalid grade ${grade} for student ${student.name} ${student.surname}`);
        }
    };
    return descriptor;
}
exports.validateGrade = validateGrade;
