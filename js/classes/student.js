"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Student = void 0;
const enums_1 = require("../types/enums");
const universityMember_1 = require("./universityMember");
const decorators_1 = require("../decorators/decorators");
class Student extends universityMember_1.UniversityMember {
    constructor(name, surname) {
        super(name, surname);
        this.subjects = [];
    }
    printGrades() {
        if (this.subjects.length > 0) {
            console.log(`Grades of ${this.name} ${this.surname}:`);
            this.subjects.forEach((subject) => {
                const grade = subject.getGradeForStudent(this);
                if (grade !== undefined) {
                    console.log(`${subject.name}: ${grade}`);
                }
            });
        }
        else {
            console.log(`${this.name} ${this.surname} doesn't have any subjects yet`);
        }
    }
    offerBribe(teacher, type, subject) {
        console.log(`Student ${this.name} ${this.surname} offers a ${type} bribe to ${teacher.name} ${teacher.surname} for ${subject.name}`);
        switch (type) {
            case enums_1.BribeType.Cash:
                this.processCashBribe(teacher, subject);
                break;
            case enums_1.BribeType.Drink:
                try {
                    this.processDrinkBribe(teacher, subject);
                }
                catch (error) {
                    console.log(error);
                }
                break;
            default:
                console.log(`Teacher ${teacher.name} ${teacher.surname} does not accept ${type} bribes.`);
                break;
        }
    }
    processCashBribe(teacher, subject) {
        console.log(`Processing cash bribe for ${teacher.name} ${teacher.surname} in ${subject.name}`);
        const grade = 90;
        console.log(`${teacher.name} ${teacher.surname} assigns a grade of ${grade} to ${this.name} ${this.surname} in ${subject.name}`);
        subject.setGradeForStudent(this, grade);
    }
    processDrinkBribe(teacher, subject) {
        console.log(`Processing drink bribe for ${teacher.name} ${teacher.surname} in ${subject.name}`);
        throw new Error(`Teacher ${teacher.name} ${teacher.surname} cannot accept a drink as a bribe.`);
    }
}
exports.Student = Student;
__decorate([
    decorators_1.logGrades
], Student.prototype, "printGrades", null);
__decorate([
    __param(2, decorators_1.required)
], Student.prototype, "offerBribe", null);
__decorate([
    __param(1, decorators_1.required)
], Student.prototype, "processCashBribe", null);
__decorate([
    decorators_1.throwErrorOnDrinkBribe,
    __param(1, decorators_1.required)
], Student.prototype, "processDrinkBribe", null);
