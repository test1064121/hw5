"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Subject = void 0;
const decorators_1 = require("../decorators/decorators");
class Subject {
    constructor(name) {
        this.name = name;
        this.teacher = null;
        this.enrolledStudents = [];
        this.grades = new Map();
    }
    getGradeForStudent(student) {
        return this.grades.get(student);
    }
    setGradeForStudent(student, grade) {
        this.grades.set(student, grade);
    }
    printEnrolledStudents() {
        console.log(`Enrolled students in ${this.name}:`);
        this.enrolledStudents.forEach((student) => {
            console.log(`${student.name} ${student.surname}`);
        });
    }
}
exports.Subject = Subject;
__decorate([
    decorators_1.logGradesForStudent
], Subject.prototype, "getGradeForStudent", null);
__decorate([
    decorators_1.validateGrade
], Subject.prototype, "setGradeForStudent", null);
__decorate([
    decorators_1.printEnrolledStudentsInfo
], Subject.prototype, "printEnrolledStudents", null);
