"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Teacher = void 0;
const universityMember_1 = require("./universityMember");
const decorators_1 = require("../decorators/decorators");
class Teacher extends universityMember_1.UniversityMember {
    constructor(name, surname) {
        super(name, surname);
        this.subjects = [];
    }
    setGrade(student, subject, grade) {
        if (subject.teacher === this &&
            subject.enrolledStudents.includes(student)) {
            subject.setGradeForStudent(student, grade);
            console.log(`${this.name} ${this.surname} assigns a grade of ${grade} to ${student.name} ${student.surname} in ${subject.name}`);
        }
        else {
            console.log(`${this.name} ${this.surname} cannot assign a grade for ${student.name} ${student.surname} in ${subject.name}`);
        }
    }
}
exports.Teacher = Teacher;
__decorate([
    __param(1, decorators_1.required)
], Teacher.prototype, "setGrade", null);
