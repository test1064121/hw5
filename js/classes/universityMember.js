"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UniversityMember = void 0;
class UniversityMember {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }
}
exports.UniversityMember = UniversityMember;
