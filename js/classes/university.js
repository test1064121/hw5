"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.University = void 0;
const decorators_1 = require("../decorators/decorators");
class University {
    constructor(name) {
        this.teachers = [];
        this.students = [];
        this.name = name;
    }
    assignTeacher(teacher, subject) {
        if (!this.teachers.includes(teacher)) {
            this.teachers.push(teacher);
        }
        teacher.subjects.push(subject);
        subject.teacher = teacher;
    }
    enrollStudent(student, subject) {
        if (!this.students.includes(student)) {
            this.students.push(student);
        }
        student.subjects.push(subject);
        subject.enrolledStudents.push(student);
    }
}
exports.University = University;
__decorate([
    __param(1, decorators_1.required)
], University.prototype, "assignTeacher", null);
__decorate([
    __param(1, decorators_1.required)
], University.prototype, "enrollStudent", null);
