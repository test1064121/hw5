import { University } from "./classes/university";
import { Student } from "./classes/student";
import { Teacher } from "./classes/teacher";
import { Subject } from "./classes/subject";
import { BribeType } from "./types/enums";

const myUni = new University("NTU KHPI");

const danyilStudent = new Student("Danyil", "Dorofieiev");

const vitaliyTeacher = new Teacher("Vitaliy", "Cal");

const mathSubject = new Subject("Math");

myUni.assignTeacher(vitaliyTeacher, mathSubject);
myUni.enrollStudent(danyilStudent, mathSubject);

danyilStudent.offerBribe(vitaliyTeacher, BribeType.Cash, mathSubject);
danyilStudent.offerBribe(vitaliyTeacher, BribeType.Drink, mathSubject);

danyilStudent.printGrades();

const grishaStudent = new Student("Grisha", "Pavlov");
const dashaStudent = new Student("Dasha", "Koroleva");

const physicsSubject = new Subject("Physics");

myUni.enrollStudent(grishaStudent, physicsSubject);
myUni.enrollStudent(dashaStudent, physicsSubject);

vitaliyTeacher.setGrade(danyilStudent, mathSubject, 70);
vitaliyTeacher.setGrade(grishaStudent, physicsSubject, 85);
vitaliyTeacher.setGrade(dashaStudent, physicsSubject, 62);

danyilStudent.printGrades();
grishaStudent.printGrades();
dashaStudent.printGrades();

mathSubject.printEnrolledStudents();
physicsSubject.printEnrolledStudents();
