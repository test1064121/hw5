import { BribeType } from "../types/enums";

export interface IUniversityMember {
  name: string;
  surname: string;
}

export interface IGradeable {
  getGradeForStudent(student: IStudent): number | undefined;
  setGradeForStudent(student: IStudent, grade: number): void;
}

export interface ISubject extends IGradeable {
  name: string;
  teacher: ITeacher | null;
  enrolledStudents: IStudent[];
  printEnrolledStudents(): void;
}

export interface IStudent extends IUniversityMember {
  subjects: ISubject[];
  printGrades(): void;
  offerBribe(teacher: ITeacher, type: BribeType, subject: ISubject): void;
}

export interface ITeacher extends IUniversityMember {
  subjects: ISubject[];
  setGrade(student: IStudent, subject: ISubject, grade: number): void;
}

export interface IUniversity {
  name: string;
  teachers: ITeacher[];
  students: IStudent[];
  assignTeacher(teacher: ITeacher, subject: ISubject): void;
  enrollStudent(student: IStudent, subject: ISubject): void;
}
